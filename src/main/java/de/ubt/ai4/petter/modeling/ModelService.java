package de.ubt.ai4.petter.modeling;

import org.camunda.bpm.model.bpmn.Bpmn;
import org.camunda.bpm.model.bpmn.BpmnModelInstance;
import org.camunda.bpm.model.bpmn.instance.Task;
import org.camunda.bpm.model.xml.instance.ModelElementInstance;
import org.camunda.bpm.model.xml.type.ModelElementType;
import org.springframework.stereotype.Service;

import java.io.File;
import java.util.Collection;

@Service
public class ModelService {

    private Collection<ModelElementInstance> getTasks(BpmnModelInstance model) {
        ModelElementType tasks = model.getModel().getType(Task.class);
        return model.getModelElementsByType(tasks);
    }

    public void getModel(String name) {
        this.getModelFromPath("src/main/resources/static/processes/" + name + "/" + name + ".bpmn");
    }

    private BpmnModelInstance getModelFromPath(String path) {
        File file  = new File(path);
        return this.getModelFromFile(file);
    }

    private BpmnModelInstance getModelFromFile(File file) {
        return Bpmn.readModelFromFile(file);
    }

}
