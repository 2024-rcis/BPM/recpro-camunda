package de.ubt.ai4.petter.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.camunda.bpm.engine.RepositoryService;
import org.camunda.bpm.engine.repository.ProcessDefinition;
import org.camunda.bpm.model.bpmn.Bpmn;
import org.camunda.bpm.model.bpmn.BpmnModelInstance;
import org.camunda.bpm.model.bpmn.instance.UserTask;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class BpmsProcessModel {
    private String id;
    private String xml;
    private List<BpmsActivity> activities = new ArrayList<>();
    private List<BpmsRole> roles = new ArrayList<>();

    public static BpmsProcessModel fromProcessDefinition(ProcessDefinition processDefinition, RepositoryService repositoryService) {
        InputStream stream = repositoryService.getProcessModel(processDefinition.getId());
        BpmnModelInstance model = Bpmn.readModelFromStream(stream);
        List<BpmsActivity> activities = BpmsActivity.getFromFlowElementCollection(model.getModelElementsByType(UserTask.class));

        return new BpmsProcessModel(
                processDefinition.getId() + "_model",
                Bpmn.convertToString(model),
                activities,
                BpmsRole.getFromModelInstance(model)
        );
    }
}