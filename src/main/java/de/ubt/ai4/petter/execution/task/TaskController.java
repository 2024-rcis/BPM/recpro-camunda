package de.ubt.ai4.petter.execution.task;

import de.ubt.ai4.petter.model.BpmsTask;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@RequestMapping("/api/execution/task/")
@AllArgsConstructor
public class TaskController {

    private RecproTaskService taskService;

    @GetMapping("{taskId}")
    public BpmsTask getRecproTaskById(@PathVariable String taskId) {
        return taskService.getById(taskId);
    }

    @PostMapping("claim/{taskId}")
    public void claimTask(@PathVariable String taskId, @RequestParam String userId) {
        taskService.claim(taskId, userId);
    }

    @PostMapping("unclaim/{taskId}")
    public void unclaimTask(@PathVariable String taskId) {
        taskService.unclaimTask(taskId);
    }

    @DeleteMapping("{taskId}")
    public void deleteTask(@PathVariable String taskId) {
        taskService.deleteTask(taskId);
    }

    @PostMapping("assign/{taskId}")
    public void assignTask(@PathVariable String taskId, @RequestParam String userId) {
        taskService.assignTask(taskId, userId);
    }

    @PostMapping("complete/{taskId}")
    public void completeTask(@PathVariable String taskId) {
        taskService.completeTask(taskId);
    }

    @PutMapping("{taskId}")
    public void setPriority(@PathVariable String taskId, @RequestParam int priority) {
        taskService.setPriority(taskId, priority);
    }

    @PostMapping("setAttribute/{taskId}")
    public void setAttribute(@PathVariable String taskId, @RequestParam String attributeId, @RequestParam Object value) {
        taskService.setAttribute(taskId, attributeId, value);
    }

    @PostMapping("setAttributes/{taskId}")
    public void setAttributes(@PathVariable String taskId, @RequestBody Map<String, Object> attributes) {
        taskService.setAttributes(taskId, attributes);
    }

}
