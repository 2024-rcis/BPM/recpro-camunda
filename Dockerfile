FROM openjdk:17
# VOLUME [ "/tmp" ]
COPY target/rs4bpm-camunda-1.0.0.jar rs4bpm-camunda-1.0.0.jar
COPY target/classes/static /static/
#COPY backend/src/main/resources/db/migrationScripts
ENTRYPOINT [ "java", "-jar", "/rs4bpm-camunda-1.0.0.jar" ]
